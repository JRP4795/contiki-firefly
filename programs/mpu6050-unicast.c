#include <math.h>
#include <stdio.h>
#include <string.h>
#include "contiki.h"
#include "net/rime/rime.h"
#include "dev/dht11.h"
#include "dev/i2c.h"
#include "dev/ioc.h"
#include "dev/gpio.h"
#include "sys/clock.h"
#include "dev/leds.h"

#define LED_PORT_BASE          GPIO_PORT_TO_BASE(GPIO_D_NUM)
#define RED_PIN_MASK           GPIO_PIN_MASK(5)
#define GREEN_PIN_MASK         GPIO_PIN_MASK(4)
/*---------------------------------------------------------------------------*/
#define Device_Address 		0x68	/*Device Address/Identifier for MPU6050*/
#define WHO_AM_I			0x75

#define PWR_MGMT_1   		0x6B
#define SMPLRT_DIV   		0x19
#define CONFIG       		0x1A
#define GYRO_CONFIG  		0x1B
#define ACCEL_CONFIG  		0x1C
#define INT_ENABLE   		0x38
#define ACCEL_XOUT_H 		0x3B
#define ACCEL_YOUT_H 		0x3D
#define ACCEL_ZOUT_H 		0x3F
#define GYRO_XOUT_H  		0x43
#define GYRO_YOUT_H  		0x45
#define GYRO_ZOUT_H  		0x47
#define TEMP_OUT_H			0x41

#define I2C_SCL_PORT        GPIO_C_NUM
#define I2C_SCL_PIN         3
#define I2C_SDA_PORT        GPIO_C_NUM
#define I2C_SDA_PIN         2
#define bus_speed			I2C_SCL_NORMAL_BUS_SPEED

int ID;
int LSB_gyro = 131;
int LSB_accel = 16384;
int pre = 100; // precision
int Acc_x, Acc_y, Acc_z, Gyro_x, Gyro_y, Gyro_z, temperature;
int Gx, Gy, Gz, Ax, Ay, Az, tp;
uint8_t query; 

/*---------------------------------------------------------------------------*/
PROCESS(mpu6050_unicast_process, "Example unicast");
AUTOSTART_PROCESSES(&mpu6050_unicast_process);
/*---------------------------------------------------------------------------*/
static void
recv_uc(struct unicast_conn *c, const linkaddr_t *from)
{
  if(((from->u8[0])-(from->u8[1])) == 16){
  	query = 1;	
  	//printf("unicast message received from %d.%d: %s\n", from->u8[0], from->u8[1], (char *)packetbuf_dataptr());
  }
}
/*---------------------------------------------------------------------------*/
static void
sent_uc(struct unicast_conn *c, int status, int num_tx)
{
  const linkaddr_t *dest = packetbuf_addr(PACKETBUF_ADDR_RECEIVER);
  if(linkaddr_cmp(dest, &linkaddr_null)) {
    return;
  }
}
/*---------------------------------------------------------------------------*/
static const struct unicast_callbacks unicast_callbacks = {recv_uc, sent_uc};
static struct unicast_conn uc;
/*---------------------------------------------------------------------------*/
static int absolute(int x){
	return (x<0) ? -(x):x;
}
/*---------------------------------------------------------------------------*/
void delay_msec(uint16_t t){
	uint16_t x;
	
	for(x=0;x<(t*10);x++){
		clock_delay_usec(100);
	}
}
/*---------------------------------------------------------------------------*/
void led_toggle(uint8_t color, uint8_t t, uint8_t x){

	uint8_t i;
	
	for(i=0; i<x; i++){
  		GPIO_SET_PIN(LED_PORT_BASE, color);
  		delay_msec(t);
  		GPIO_CLR_PIN(LED_PORT_BASE, color);
  		delay_msec(t);
  	}
}
/*---------------------------------------------------------------------------*/
static int read_raw_data1(uint8_t reg){

	uint8_t buf[1];
	
  	i2c_master_enable();
  	if(i2c_single_send(Device_Address, reg) == I2C_MASTER_ERR_NONE){
  		if(i2c_burst_receive(Device_Address, &buf[0], 1) == I2C_MASTER_ERR_NONE){
  			return buf[0];
  		}
  		else return 0;
  	}
  	else return 0;
}
/*---------------------------------------------------------------------------*/
static uint8_t write_data(uint8_t ID, uint8_t reg, uint8_t data){

	uint8_t buf[2];
	buf[0] = reg;
	buf[1] = data;
	
	i2c_master_enable();
	i2c_burst_send(ID, buf, 2);
	
	return 0;
}
/*---------------------------------------------------------------------------*/
void mpu6050_init(){
	
	write_data(ID, SMPLRT_DIV, 0x07);
	write_data(ID, PWR_MGMT_1, 0x01);
	write_data(ID, CONFIG, 0);
	write_data(ID, GYRO_CONFIG, 0x00);
	write_data(ID, ACCEL_CONFIG, 0x00);
	write_data(ID, INT_ENABLE, 0x01);	

} 
/*---------------------------------------------------------------------------*/
static int read_raw_data2(uint8_t reg){

	uint8_t high_byte, low_byte, rawdata; 
	uint16_t value;
	int ret;
	
	rawdata = high_byte = low_byte = value = 0;
	
	i2c_master_enable();
	if(i2c_single_send(Device_Address, reg) == I2C_MASTER_ERR_NONE){
		if(i2c_burst_receive(Device_Address, &rawdata, 1) == I2C_MASTER_ERR_NONE){
			high_byte = rawdata;
		}
	}
	rawdata = 0;
	i2c_master_enable();
	if(i2c_single_send(Device_Address, reg+1) == I2C_MASTER_ERR_NONE){
		if(i2c_burst_receive(Device_Address, &rawdata, 1) == I2C_MASTER_ERR_NONE){
			low_byte = rawdata;
		}
	}
	value = (high_byte << 8) + low_byte;
	if((value & 0x8000) > 0) {
		value = (~value + 1);
		ret = 0 - value;
    	return ret;
  	}
  	else{return value;}
}
/*---------------------------------------------------------------------------*/
void prepare_data(){
	Acc_x = read_raw_data2(ACCEL_XOUT_H);
    Acc_y = read_raw_data2(ACCEL_YOUT_H);
    Acc_z = read_raw_data2(ACCEL_ZOUT_H);
    
    Gyro_x = read_raw_data2(GYRO_XOUT_H);
    Gyro_y = read_raw_data2(GYRO_YOUT_H);
    Gyro_z = read_raw_data2(GYRO_ZOUT_H);
    
    temperature = read_raw_data2(TEMP_OUT_H);
    
    Ax = (pre*Acc_x) / LSB_accel;
	Ay = (pre*Acc_y) / LSB_accel;
	Az = (pre*Acc_z) / LSB_accel;
		
	Gx = (pre*Gyro_x) / LSB_gyro;
	Gy = (pre*Gyro_y) / LSB_gyro;
	Gz = (pre*Gyro_z) / LSB_gyro;
	
	tp = ((pre*temperature) / 340) + 3653;
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(mpu6050_unicast_process, ev, data)
{
  PROCESS_EXITHANDLER(unicast_close(&uc);)
    
  PROCESS_BEGIN();
  
  char buffer[58];
  int j;
  
  ID = 0;
  Acc_x = Acc_y = Acc_z = temperature = 0;
  Gyro_x = Gyro_y = Gyro_z = 0;
  
  GPIO_SET_OUTPUT(LED_PORT_BASE, GREEN_PIN_MASK);
  GPIO_SET_OUTPUT(LED_PORT_BASE, RED_PIN_MASK);
  
  i2c_init(I2C_SDA_PORT, I2C_SDA_PIN, I2C_SCL_PORT, I2C_SCL_PIN, bus_speed);
  
  if(read_raw_data1(WHO_AM_I) == Device_Address){
  	ID = read_raw_data1(WHO_AM_I);
  	led_toggle(GREEN_PIN_MASK, 100, 3);	
  }
  else{
  	led_toggle(RED_PIN_MASK, 100, 3);
  	printf("Error, not the Exected Address\n");
  }
  
  query = 0;
  mpu6050_init();
  unicast_open(&uc, 146, &unicast_callbacks);

  while(1) {
  
    static struct etimer et;
    linkaddr_t addr;
    
    etimer_set(&et, CLOCK_SECOND*0.5);
    
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
    
    prepare_data();
    
    if(query == 1){	
    
    	packetbuf_clear();
    	snprintf(buffer, 58,"%d.%d,%d.%d,%d.%d,deg/s,%d.%d,%d.%d,%d.%d,g,%d.%d,degC",Gx/100, absolute(Gx%100), Gy/100, absolute(Gy%100), Gz/100, absolute(Gz%100), Ax/100, absolute(Ax%100), Ay/100, absolute(Ay%100), Az/100, absolute(Az%100), tp/100, absolute(tp%100));
    	j = strlen(buffer);
    	j = j + 2;
    	snprintf(buffer, j ,"%d.%d,%d.%d,%d.%d,deg/s,%d.%d,%d.%d,%d.%d,g,%d.%d,degC",Gx/100, absolute(Gx%100), Gy/100, absolute(Gy%100), Gz/100, absolute(Gz%100), Ax/100, absolute(Ax%100), Ay/100, absolute(Ay%100), Az/100, absolute(Az%100), tp/100, absolute(tp%100));
    	led_toggle(GREEN_PIN_MASK, 10, 1);
    	printf("%s\n", buffer);
    	packetbuf_copyfrom(&buffer,j);
    	addr.u8[0] = 0xe4;					// Client rime configured address e4:d4
    	addr.u8[1] = 0xd4;
    	if(!linkaddr_cmp(&addr, &linkaddr_node_addr)) {
      		unicast_send(&uc, &addr);
    	}
    	query = 0;
    }
    
  }
  PROCESS_END();
}
