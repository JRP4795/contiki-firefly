
#include <stdio.h>
#include <sys/rtimer.h>
#include <string.h>
#include "contiki.h"
#include "net/rime/rime.h"
#include "dev/dht11.h"
#include "sys/clock.h"
#include "dev/gpio.h"

#define LED_PORT_BASE          GPIO_PORT_TO_BASE(GPIO_D_NUM)
#define RED_PIN_MASK           GPIO_PIN_MASK(5)
#define GREEN_PIN_MASK         GPIO_PIN_MASK(4)
#define BLUE_PIN_MASK          GPIO_PIN_MASK(3)

uint8_t t;
/*---------------------------------------------------------------------------*/
PROCESS(client_unicast_process, "Example unicast");
AUTOSTART_PROCESSES(&client_unicast_process);
/*---------------------------------------------------------------------------*/
void delay_msec(uint16_t t){
	uint16_t x;
	for(x=0;x<(t*10);x++){
		clock_delay_usec(100);
	}
}
/*---------------------------------------------------------------------------*/
void led_toggle(uint8_t color, uint8_t t, uint8_t x){

	uint8_t i;
	for(i=0; i<x; i++){
  		GPIO_SET_PIN(LED_PORT_BASE, color);
  		delay_msec(t);
  		GPIO_CLR_PIN(LED_PORT_BASE, color);
  		delay_msec(t);
  	}
}
/*---------------------------------------------------------------------------*/
static void
recv_uc(struct unicast_conn *c, const linkaddr_t *from)
{
  printf("%02X%02X, %s\n", from->u8[0], from->u8[1], (char *)packetbuf_dataptr());
  packetbuf_clear();
  led_toggle(GREEN_PIN_MASK, 5, 1);
}
/*---------------------------------------------------------------------------*/
static void
sent_uc(struct unicast_conn *c, int status, int num_tx)
{
  const linkaddr_t *dest = packetbuf_addr(PACKETBUF_ADDR_RECEIVER);
  if(linkaddr_cmp(dest, &linkaddr_null)) {
    return;
  }
}
/*---------------------------------------------------------------------------*/
static const struct unicast_callbacks unicast_callbacks = {recv_uc, sent_uc};
static struct unicast_conn uc;
static struct etimer et;
/*---------------------------------------------------------------------------*/
static void send_command(uint8_t command){
	linkaddr_t addr;
	switch(command){
	
		case 52:
			packetbuf_copyfrom("52",3);
    		addr.u8[0] = 0xe2;					//  S/N: 052
    		addr.u8[1] = 0x33;
    		if(!linkaddr_cmp(&addr, &linkaddr_node_addr)) {
      			unicast_send(&uc, &addr);
      			packetbuf_clear();
    		}
    		return;
		case 15:
			packetbuf_copyfrom("15",3);
    		addr.u8[0] = 0xe3;					//  S/N: 015
    		addr.u8[1] = 0x7e;
    		if(!linkaddr_cmp(&addr, &linkaddr_node_addr)) {
      			unicast_send(&uc, &addr);
      			packetbuf_clear();
    		}
    		return;
		case 57: 
			packetbuf_copyfrom("57",3);
    		addr.u8[0] = 0xc7;					//  S/N: 057
    		addr.u8[1] = 0xf2;
    		if(!linkaddr_cmp(&addr, &linkaddr_node_addr)) {
      			unicast_send(&uc, &addr);
      			packetbuf_clear();
    		}
		default: return;
	}
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(client_unicast_process, ev, data)
{
  PROCESS_EXITHANDLER(unicast_close(&uc);)
  PROCESS_BEGIN();
  t = 0;
  unicast_open(&uc, 146, &unicast_callbacks);

  while(1) {
    
    etimer_set(&et, CLOCK_SECOND);
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
    t++;
	if(t==5) { send_command(52);}
    if(t==10){ send_command(15);}
    if(t==15){ send_command(57); t = 0;}
  }
  PROCESS_END();
}
